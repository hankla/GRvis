import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from reduce_data_utils import *
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_radial_flux_through_shells_over_r(setup, times, flux_type, **kwargs):
    """
    This script will calculate (if necessary) and plot the
    specified spherical flux (integrated over radial shells)
    at given times over radius (on the same plot).

    Spherical flux is defined in White, Stone, Quataert 2019 Eq. 5 (NOTE: currently not normalized to sqrt(4 pi) or 2)

    Angular momentum flux is defined in Eq. 9 of White, Quataert, & Gammie 2020.

    INPUTS:
    - setup: a setup_manager that has all the path information.
    - times: the timesteps to be loaded, i.e. an array of integers.
    - flux_type: options are "mass_flux", mag_flux", "norm_ang_mom_flux", "norm_energy_flux"
    KWARGS:
    - overwrite_data: if True, then re-calculate/overwrite the data.
        Otherwise, try to load data from a file if it exists.
    - overwrite_figure: if True, then overwrite any figure with the same name.
        Otherwise, check if figure exists and don't overwrite.
        XX NOT IMPLEMENTED YET XX
    - figure_name: if specified, save figure with this name. If not specified,
        save figure will pre-built name. If "show", then plt.show().
    - four_pi_norm: if True, multiply by sqrt(4*pi)/2 as in WSQ19

    TO DO:
    - add option to time average
    - add sqrt(4 pi) and 1/2 (option)

    """
    flux_labels = {"mass_flux": r"$-\dot M$", "mag_flux":r"$\Phi$", "norm_ang_mom_flux":r"$\dot J/\dot M$", "norm_energy_flux":r"$\dot E/\dot M$"}
    flux_strings = {"mass_flux": "Mass Flux (-1.0 * Accretion Rate)", "mag_flux":"Magnetic Flux", "norm_ang_mom_flux":"Angular momentum flux (normalized to mass flux)", "norm_energy_flux":r"Energy flux (normalized to mass flux)"}

    overwrite_figure = kwargs.get("overwrite_figure", False)
    overwrite_data = kwargs.get("overwrite_data", False)
    four_pi_norm = kwargs.get("four_pi_norm", False)

    figure_name = kwargs.get("figure_name", None)
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    figure_dir = setup.path_to_figures + "fluxes-spherical_over_r/" + flux_type + "/"
    if four_pi_norm:
        figure_dir += "fourPiNorm/"
    figure_name = figure_dir + flux_type + "_through_shells_over_r_t"


    x1v = retrieve_coords(setup, times[0], overwrite_data=overwrite_data)["x1v"]
    time_dict = retrieve_time_dict(setup, overwrite_data=overwrite_data)

    # Set up color map for different times
    colors = plt.cm.viridis(np.linspace(0, 1, len(times)))

    for i, time in enumerate(times):
        figure_name += "-{:05d}".format(time)
        # ----- First have to get data ------
        flux_over_r = retrieve_spherical_flux_values_over_r(flux_type, setup, time, overwrite_data=overwrite_data)
        if four_pi_norm:
            flux_over_r = np.sqrt(4.0*np.pi)*flux_over_r/2.0
        sim_time = time_dict[time]
        label = "$t={:.2f}~GM/c^3$".format(sim_time)

        # ----- Now the actual plotting ------
        plt.plot(x1v, flux_over_r, marker="o", markersize=3, label=label, color=colors[i])

    plt.xlabel(r"$r/r_g$")
    if four_pi_norm:
        plt.ylabel(flux_labels[flux_type] + "$\sqrt{4\pi}/2$")
    else:
        plt.ylabel(flux_labels[flux_type])
    titstr = setup.sim_name
    titstr += "\n" + flux_strings[flux_type] + " through each radial shell"
    plt.legend()
    plt.title(titstr)
    plt.tight_layout()

    figure_name = kwargs.get("figure_name", figure_name)
    if figure_name == "show":
        plt.show()
    else:
        if not os.path.isdir(figure_dir):
            os.makedirs(figure_dir)
        plt.savefig(figure_name + ".png", bbox_inches='tight')
        print("Saving figure " + figure_name + ".png")

    plt.close()

if __name__ == '__main__':
    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torBeta_br32x32x64rl2x2"
    # sim_name = "1.1.1-torus2_b-gz2_a0beta500torBeta_br32x32x64rl2x2"
    # overwrite_data = False

    configuration = "lia-d"
    project = "torus_toroidal-flux"
    config = "19.0-gr-torus2_gz4"
    specs = "a85beta500torBeta_br32x32x64rl2x3"
    sim_name = config + "_" + specs
    # flux_type = "mass_flux"
    # flux_type = "norm_energy_flux"
    flux_type = "norm_ang_mom_flux"
    flux_type = "mag_flux"

    setup = setup_manager(configuration, project, sim_name)
    times = get_list_of_time_steps(setup)
    times = [250, 500, 750, 1000]
    # times = [10, 20, 30]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    # kwargs["overwrite_data"] = overwrite_data
    # kwargs["four_pi_norm"] = True

    plot_radial_flux_through_shells_over_r(setup, times, flux_type, **kwargs)

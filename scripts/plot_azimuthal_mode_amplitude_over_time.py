import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from reduce_data_utils import *
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs):
    """
    This script will calculate (if necessary) and plot the
    magnetic flux at given times over radius (on the same plot).
    Note this is NOT normalized! This will need to be fixed.

    Magnetic flux is defined in Eq. XX

    INPUTS:
    - setup: a setup_manager that has all the path information.
    - times: the timesteps to be loaded, i.e. an array of integers.
    KWARGS:
    - overwrite_data: if True, then re-calculate/overwrite the data.
        Otherwise, try to load data from a file if it exists.
    - overwrite_figure: if True, then overwrite any figure with the same name.
        Otherwise, check if figure exists and don't overwrite.
        XX NOT IMPLEMENTED YET XX
    - figure_name: if specified, save figure with this name. If not specified,
        save figure will pre-built name. If "show", then plt.show().

    TO DO:
    - add option to time average
    - normalize to some standard

    """
    radius = kwargs.get("radius", 10.0)
    theta = kwargs.get("theta", np.pi/2.0)
    m_start = kwargs.get("m_start", 1)
    m_end = kwargs.get("m_end", 3)
    overwrite_figure = kwargs.get("overwrite_figure", False)
    overwrite_data = kwargs.get("overwrite_data", False)

    figure_dir = setup.path_to_figures + "azimuthal_mode_amplitude_over_t/" + quantity + "/"
    figure_dir += "r{}/".format(radius)
    figure_name = figure_dir + quantity + "_m{}-{}-amplitude_r{}.png".format(m_start, m_end, radius)

    time_dict = retrieve_time_dict(setup, overwrite_data=overwrite_data)
    mode_amplitudes = retrieve_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs)

    plt.figure()
    num_plots = m_end - m_start + 1
    colormap = plt.cm.viridis
    plt.gca().set_prop_cycle('color', colormap(np.linspace(0, 1, num_plots)))
    labels = []

    for i in np.arange(m_start, m_end + 1, 1):
        plt.plot(times, mode_amplitudes[:, i+1])
        labels.append(r"$m = {}$".format(i))

    plt.xlabel("Time $[GM/c^3]$")
    plt.legend(labels, ncol=8)
    plt.ylabel("Mode amplitude")
    plt.title(setup.sim_name + "\n" + quantity + "\n $r={}~[GM/c^2]$".format(radius))
    plt.tight_layout()

    figure_name = kwargs.get("figure_name", figure_name)
    if figure_name == "show":
        plt.show()
    else:
        if not os.path.isdir(figure_dir):
            os.makedirs(figure_dir)
        plt.savefig(figure_name, bbox_inches='tight')
        print("Saving figure " + figure_name + ".png")
    plt.close()



if __name__ == '__main__':
    configuration = "lia-d"
    project = "torus_toroidal-flux"
    config = "19.0-gr-torus2_gz4"
    specs = "a85beta500torBeta_br32x32x64rl2x3"

    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torBeta_br32x32x64rl2x2"

    sim_name = config + "_" + specs
    overwrite_data = False

    setup = setup_manager(configuration, project, sim_name)
    times = get_list_of_time_steps(setup)
    quantity = "Bcc3"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data

    plot_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs)

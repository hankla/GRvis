#!/bin/bash
#SBATCH --time=48:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=analysis
#SBATCH --output=logs/out_plot_analysis.%j
#SBATCH --error=logs/err_plot_analysis.%j
#SBATCH -A TG-AST190020

# --------------------------------------
# USAGE
# 1. Set user inputs below and edit script.py to desired specifications.
# 2. Change above SBATCH settings.
# 3. Make sure logs exists in this directory! i.e. run "mkdir -p logs"
# 4. From the command line (in the sbatch_scripts/ directory), run
#         sbatch sbatch_python_script.sh
# --------------------------------------

# --------------------------------------
# USER INPUTS
# NOTE: all other parameters will have to be changed within the
# ../scripts/script.py file!!
script='plot_mdot_over_r'
project='torus_vert-flux'
sim_name='vert-flux_initial'
# --------------------------------------

# --------------------------------------
# NO EDITING NEEDED BELOW THIS LINE
# --------------------------------------
module purge
module load intel/18.0.2
module load python3/3.7.0
module list

scriptpath="$WORK/GRvis/scripts/"
logpath=$(pwd)"/logs/log_${script}.txt"

fname="${script}.py"
copyname="${script}_copy.py"

cd ${scriptpath}
date

# first, replace script configuration, project name, and simulation name
python3 make_script_copy.py ${fname} "$project" "$sim_name"

cd script_copies/
pwd
echo ${copyname}
echo ${logpath}

python3 ${copyname}
date

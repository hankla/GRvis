import os
import sys
sys.path.append("../modules/")
import raw_data_utils as rdu
import calculate_data_utils as cdu
from metric_utils import *
import pickle

def get_list_of_time_steps(setup):
    time_steps = []
    for file in os.listdir(setup.path_to_raw_data):
        if file.split(".")[-1] == "athdf":
            time_steps.append(int(file.split(".")[-2]))
    time_steps.sort()
    return time_steps

def reduce_time_dict(setup):
    print("Reducing time data")
    time_dict_path = setup.path_to_reduced_data + "time_dict.p"
    time_steps = get_list_of_time_steps(setup)
    time_dict = {}

    for time in time_steps:
        data_path = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)
        raw_data = rdu.read_athdf(data_path, quantities=[])
        time_dict[time] = raw_data["Time"]
    pickle.dump(time_dict, open(time_dict_path, "wb"))
    return

def retrieve_time_dict(setup, **kwargs):
    overwrite = kwargs.get("overwrite_data", False)
    time_dict_path = setup.path_to_reduced_data + "time_dict.p"
    if overwrite or not os.path.exists(time_dict_path):
        print("Finding time data")
        reduce_time_dict(setup)

    time_dict = pickle.load(open(time_dict_path, "rb"))
    return time_dict

def retrieve_coords(setup, time=0, **kwargs):
    """
    Returns a dictionary of the coordinates.
    """
    overwrite = kwargs.get("overwrite_data", False)
    coords_path = setup.path_to_reduced_data + "coords.p"
    if overwrite or not os.path.exists(coords_path):
        print("Finding coordinate data")
        reduce_coords(setup, time)

    coords = pickle.load(open(coords_path, "rb"))
    return coords

def reduce_coords(setup, time=0):
    """
    Returns a dictionary of the coordinates.
    """
    print("Reducing coordinate data")
    coords_path = setup.path_to_reduced_data + "coords.p"
    data_path = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)

    raw_data = rdu.read_athdf(data_path, quantities=[])
    coords = {"x1v":raw_data["x1v"], "x2v":raw_data["x2v"], "x3v":raw_data["x3v"], "x1f":raw_data["x1f"], "x2f":raw_data["x2f"], "x3f":raw_data["x3f"]}
    pickle.dump(coords, open(coords_path, "wb"))
    return



def retrieve_spherical_flux_values_over_r(flux, setup, time, **kwargs):
    """
    Get the reduced data of flux values over radius.
    flux:
        - mag_flux
        - mass_flux
        - norm_energy_flux
        - norm_ang-mom_flux
    """
    overwrite_data = kwargs.get("overwrite_data", False)
    flux_path = setup.path_to_reduced_data + flux + "_through_shells_over_r/" + flux + "_through_shells_over_r_at_t{:05}.txt".format(time)
    if overwrite_data or not os.path.exists(flux_path):
        print("Calculating " + flux + " data")
        reduce_spherical_flux_values_over_r(flux, setup, time)

    flux_values_at_t = np.loadtxt(flux_path)
    return flux_values_at_t


def reduce_spherical_flux_values_over_r(flux, setup, time):
    """
    Calculate flux values.
    flux:
        - mag_flux
        - mass_flux
        - norm_energy_flux
        - norm_ang_mom_flux
    """
    print("Reducing " + flux + " for time {}".format(time))
    reduced_path = setup.path_to_reduced_data

    if flux == "mass_flux":
        quantities = ["rho", "vel1", "vel2", "vel3"]
    elif flux == "mag_flux":
        quantities = ["Bcc1"]
    elif flux == "norm_energy_flux":
        quantities = ["rho", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]
    elif flux == "norm_ang_mom_flux":
        quantities = ["rho", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]

    raw_path = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)
    raw_data = rdu.read_athdf(raw_path, quantities=quantities)

    sim_time = raw_data["Time"]
    x1v = raw_data["x1v"]
    x2v = raw_data["x2v"]
    x3v = raw_data["x3v"]
    metric = kerrschild(x1v, x2v, x3v)
    coords = (x1v, x2v, x3v)

    if flux != "mag_flux":
        out_vel = (raw_data["vel1"], raw_data["vel2"], raw_data["vel3"])
        four_vel = metric.get_four_velocity_from_output(out_vel)
        if flux != "mass_flux":
            out_B = (raw_data["Bcc1"], raw_data["Bcc2"], raw_data["Bcc3"])
            proj_b = metric.get_proj_bfield_from_outputB_fourV(out_B, four_vel)

    if flux == "mass_flux":
        flux_over_r = cdu.calculate_mass_flux_over_radial_shells((raw_data["rho"], four_vel[1], coords))
    elif flux == "mag_flux":
        flux_over_r = cdu.calculate_magnetic_flux_over_radial_shells((raw_data["Bcc1"], coords))
    elif flux == "norm_ang_mom_flux":
        mass_flux_over_r = cdu.calculate_mass_flux_over_radial_shells((raw_data["rho"], four_vel[1], coords))
        # calculate T^1_3
        Trphi = cdu.calculate_Trphi((four_vel, proj_b, coords))
        # Remember Jdot = - Trphi flux
        ang_mom_flux_over_r = -1.0*cdu.calculate_flux_over_radial_shells((Trphi, coords))
        flux_over_r = ang_mom_flux_over_r/mass_flux_over_r
    elif flux == "norm_energy_flux":
        # calculate mass flux
        mass_flux_over_r = cdu.calculate_mass_flux_over_radial_shells((raw_data["rho"], four_vel[1], coords))
        # calculate T^1_0
        Trt = cdu.calculate_Trt((four_vel, proj_b, coords))
        energy_flux_over_r = cdu.calculate_flux_over_radial_shells((Trt, coords))
        flux_over_r = energy_flux_over_r/mass_flux_over_r

    if not os.path.exists(setup.path_to_reduced_data + flux + "_through_shells_over_r/"):
        os.makedirs(setup.path_to_reduced_data + flux + "_through_shells_over_r/")
    reduced_data_fp = setup.path_to_reduced_data + flux + "_through_shells_over_r/" + flux + "_through_shells_over_r_at_t{:05}.txt".format(time)
    print(reduced_data_fp)

    x1vstr = ', '.join(map(str, raw_data["x1v"]))
    np.savetxt(reduced_data_fp, flux_over_r, header=x1vstr)
    return

def retrieve_quality_factor_phi_slice(setup, time, **kwargs):
    overwrite_data = kwargs.get("overwrite_data", False)
    write_to_file = kwargs.get("write_to_file", True)

    qf_dir = setup.path_to_reduced_data + "quality_factor_phi/"
    qf_path = qf_dir + "quality_factor_phi_at_t{:05}.p".format(time)
    if overwrite_data or not os.path.exists(qf_path):
        print("Calculating quality factor data")
        if not os.path.isdir(qf_dir):
            os.makedirs(qf_dir)
        qf_values_at_t = reduce_quality_factor_phi_slice(setup, time)
    else:
        qf_values_at_t = pickle.load(open(qf_path, "rb"))
    if write_to_file:
        pickle.dump(qf_values_at_t, open(qf_path, "wb"))

    return qf_values_at_t

def reduce_quality_factor_phi_slice(setup, time):
    print("Reducing quality factor phi for time {}".format(time))
    # ----- First have to get data ------
    filename = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)
    raw_data = rdu.read_athdf(filename, quantities=["rho", "vel1", "vel2", "vel3", "press", "Bcc1", "Bcc2", "Bcc3"])
    out_vel = (raw_data["vel1"], raw_data["vel2"], raw_data["vel3"])
    out_B = (raw_data["Bcc1"], raw_data["Bcc2"], raw_data["Bcc3"])
    press = raw_data["press"]
    density = raw_data["rho"]

    sim_time = raw_data["Time"]
    x1v = raw_data["x1v"]
    x2v = raw_data["x2v"]; nx2 = x2v.size
    x3v = raw_data["x3v"]; nx3 = x3v.size

    metric = kerrschild(x1v, x2v, x3v)
    coords = (x1v, x2v, x3v)

    four_velocity = metric.get_four_velocity_from_output(out_vel)
    proj_b = metric.get_proj_bfield_from_outputB_fourV(out_B, four_velocity)

    quality_factor_phi = cdu.calculate_quality_factor_phi((proj_b, press, density, coords))
    return quality_factor_phi


def reduce_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs):
    print("Reducing azimuthal mode amplitude for quantity " + quantity)
    times = get_list_of_time_steps(setup)
    theta = kwargs.get("theta", np.pi/2.0) # default is midplane
    radius = kwargs.get("radius", 10.0) # default is pressure max
    wavelengths = None
    ft_dir = setup.path_to_reduced_data + "azimuthal_mode_amplitude_over_time/" + quantity + "/"
    ft_path = ft_dir + "azimuthal_mode_amplitude_" + quantity + "_r{}.txt".format(radius)
    if not os.path.isdir(ft_dir):
        os.makedirs(ft_dir)

    for time in times:
        # ----- First have to get data ------
        filename = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)
        raw_data = rdu.read_athdf(filename, quantities=[quantity], x1_min=radius, x1_max=radius, x2_min=theta, x2_max=theta+0.01)
        phi_values = raw_data['x3v']
        N_phi = len(phi_values)
        phi_profile = raw_data[quantity].reshape((N_phi,))
        dphi = phi_values[1] - phi_values[0]

        # Do a spatial fourier transform of the values as a function of phi, normalized
        data_fft = np.fft.fft(phi_profile)/N_phi
        if wavelengths is None:
            wavenumbers = np.linspace(0, 1.0/dphi, N_phi)
            wavelengths = 1.0/wavenumbers

        code_time = np.array(raw_data['Time']).reshape(1,)
        output_data = np.hstack([code_time, np.abs(data_fft[:N_phi//2])]).reshape((1, N_phi//2 + 1))

        if not os.path.isfile(ft_path):
            header = "time, m=0 amplitude"
            for i in np.arange(1, N_phi//2):
                header += ", m={} amplitude".format(i)
        with open(ft_path, "a") as f:
            np.savetxt(f, output_data, header=header)

    # clean up by removing duplicates and sorting
    with open(ft_path, "r") as f:
        # first get header:
        header = f.readline()
        amplitude_data = np.loadtxt(f)
    unique_times, inds = np.unique(amplitude_data[:, 0], return_index=True)
    sorted_amplitudes = amplitude_data[inds]

    with open(ft_path, "w") as f:
        np.savetxt(f, sorted_amplitudes, header=header)

    return sorted_amplitudes



def retrieve_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs):
    overwrite_data = kwargs.get("overwrite_data", False)
    radius = kwargs.get("radius", 10.0)
    theta = kwargs.get("theta", np.pi/2.0)

    ft_dir = setup.path_to_reduced_data + "azimuthal_mode_amplitude_over_time/" + quantity + "/"
    ft_path = ft_dir + "azimuthal_mode_amplitude_" + quantity + "_r{}.txt".format(radius)

    if overwrite_data or not os.path.exists(ft_path):
        print("Calculating fourier transform profile data")
        mode_amplitudes = reduce_azimuthal_mode_amplitude_over_time(setup, quantity, **kwargs)
    else:
        mode_amplitudes = np.loadtxt(open(ft_path, "r"))

    return mode_amplitudes

def retrieve_midplane_flux_values_over_r(flux, setup, time, **kwargs):
    """
    Get the reduced data of vertical flux values through the midplane.
    flux:
        - mag_flux
        - mass_flux
        - norm_energy_flux
        - norm_ang-mom_flux
    """
    overwrite_data = kwargs.get("overwrite_data", False)
    flux_path = setup.path_to_reduced_data + flux + "_through_midplane_over_r/" + flux + "_through_midplane_over_r_at_t{:05}.txt".format(time)
    if overwrite_data or not os.path.exists(flux_path):
        print("Calculating " + flux + " data")
        reduce_midplane_flux_values_over_r(flux, setup, time)

    flux_values_at_t = np.loadtxt(flux_path)
    return flux_values_at_t


def reduce_midplane_flux_values_over_r(flux, setup, time):
    """
    Calculate flux values through midplane
    flux:
        - mag_flux
        - mass_flux
        - norm_energy_flux
        - norm_ang_mom_flux
    """
    print("Reducing " + flux + " through midplane for time {}".format(time))
    reduced_path = setup.path_to_reduced_data

    if flux == "mass_flux":
        quantities = ["rho", "vel1", "vel2", "vel3"]
    elif flux == "mag_flux":
        quantities = ["Bcc2"]
    elif flux == "norm_energy_flux":
        quantities = ["rho", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]
    elif flux == "norm_ang_mom_flux":
        quantities = ["rho", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]

    raw_path = setup.path_to_raw_data + setup.sim_name + ".prim.{:05}.athdf".format(time)
    raw_data = rdu.read_athdf(raw_path, quantities=quantities)

    sim_time = raw_data["Time"]
    x1v = raw_data["x1v"]
    x2v = raw_data["x2v"]
    x3v = raw_data["x3v"]
    metric = kerrschild(x1v, x2v, x3v)
    coords = (x1v, x2v, x3v)

    if flux != "mag_flux":
        out_vel = (raw_data["vel1"], raw_data["vel2"], raw_data["vel3"])
        four_vel = metric.get_four_velocity_from_output(out_vel)
        if flux != "mass_flux":
            out_B = (raw_data["Bcc1"], raw_data["Bcc2"], raw_data["Bcc3"])
            proj_b = metric.get_proj_bfield_from_outputB_fourV(out_B, four_vel)

    if flux == "mass_flux":
        flux_over_r = cdu.calculate_flux_through_midplane_disk((raw_data["rho"]*four_vel[1], coords))
    elif flux == "mag_flux":
        flux_over_r = cdu.calculate_flux_through_midplane_disk((raw_data["Bcc2"], coords))
    elif flux == "norm_ang_mom_flux":
        mass_flux_over_r = cdu.calculate_flux_through_midplane_disk((raw_data["rho"]*four_vel[1], coords))
        # calculate T^1_3
        Trphi = cdu.calculate_Trphi((four_vel, proj_b, coords))
        # Remember Jdot = - Trphi flux
        ang_mom_flux_over_r = -1.0*cdu.calculate_flux_through_midplane_disk((Trphi, coords))
        flux_over_r = ang_mom_flux_over_r/mass_flux_over_r
    elif flux == "norm_energy_flux":
        # calculate mass flux
        mass_flux_over_r = cdu.calculate_flux_through_midplane_disk((raw_data["rho"]*four_vel[1], coords))
        # calculate T^1_0
        Trt = cdu.calculate_Trt((four_vel, proj_b, coords))
        energy_flux_over_r = cdu.calculate_flux_through_midplane_disk((Trt, coords))
        flux_over_r = energy_flux_over_r/mass_flux_over_r

    if not os.path.exists(setup.path_to_reduced_data + flux + "_through_midplane_over_r/"):
        os.makedirs(setup.path_to_reduced_data + flux + "_through_midplane_over_r/")
    reduced_data_fp = setup.path_to_reduced_data + flux + "_through_midplane_over_r/" + flux + "_through_midplane_over_r_at_t{:05}.txt".format(time)
    print(reduced_data_fp)

    x1vstr = ', '.join(map(str, raw_data["x1v"]))
    np.savetxt(reduced_data_fp, flux_over_r, header=x1vstr)
    return

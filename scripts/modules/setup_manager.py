import os

class setup_manager:
    """
    configuration: determines the paths, etc. depending on the specific set-up.
    """
    def __init__(self, configuration, project, sim_name, **kwargs):
        self.configuration = configuration
        self.project = project
        self.sim_name = sim_name

        # ------ Add setup information here
        if configuration == "lia-hp":
            overlord_path = "/mnt/c/Users/liaha/research/projects/" + project + "/"
            path_to_reduced_data = overlord_path + "reduced_data/" + sim_name + "/"
            path_to_raw_data = overlord_path + "raw_data/" + sim_name + "/"
            path_to_figures = overlord_path + "figures/" + sim_name + "/"

        elif configuration == "lia-d":
            overlord_path = "/mnt/d/" + project + "/"
            path_to_reduced_data = overlord_path + "reduced_data/" + sim_name + "/"
            path_to_raw_data = overlord_path + "raw_data/" + sim_name + "/"
            path_to_figures = overlord_path + "figures/" + sim_name + "/"

        elif configuration == "stampede2":
            overlord_path = "/work/06165/ahankla/" + project + "/"
            path_to_reduced_data = overlord_path + "data_reduced/" + sim_name + "/"
            path_to_raw_data = "/scratch/06165/ahankla/" + project + "/data_raw/" + sim_name + "/"
            path_to_figures = overlord_path + "figures/" + sim_name + "/"

        elif configuration == "ellie":
            path_to_reduced_data = "C:/Users/Ellie/Downloads/nerd/SRSData/Reduced/" + sim_name + "/"
            path_to_raw_data = "C:/Users/Ellie/Downloads/nerd/SRSData/" + sim_name + "/"
            path_to_figures = "C:/Users/Ellie/Downloads/nerd/SRSPlots/" + sim_name + "/"

        # ----------------------------------------
        # If paths are nonstandard, add them here
        # ----------------------------------------
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data", path_to_reduced_data)
        self.path_to_raw_data = kwargs.get("path_to_raw_data", path_to_raw_data)
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)

        # ----------------------------------------
        # Create the directories if need be
        # ----------------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        self.hst_file = self.path_to_raw_data + self.sim_name + ".hst"

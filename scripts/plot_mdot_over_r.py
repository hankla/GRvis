import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from reduce_data_utils import *
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_mdot_over_r(setup, times, **kwargs):
    """
    This script will calculate (if necessary) and plot the
    accretion rate at given times over radius (on the same plot).
    Note this is NOT normalized! This will need to be fixed.

    INPUTS:
    - setup: a setup_manager that has all the path information.
    - times: the timesteps to be loaded, i.e. an array of integers.
    KWARGS:
    - overwrite_data: if True, then re-calculate/overwrite the data.
        Otherwise, try to load data from a file if it exists.
    - overwrite_figure: if True, then overwrite any figure with the same name.
        Otherwise, check if figure exists and don't overwrite.
        XX NOT IMPLEMENTED YET XX
    - figure_name: if specified, save figure with this name. If not specified,
        save figure will pre-built name. If "show", then plt.show().

    TO DO:
    - add option to time average
    - normalize to some standard

    """
    plt.figure()
    figure_dir = setup.path_to_figures + "fluxes_over_r/"
    figure_name = figure_dir + "mdot_flux_over_r_t"

    overwrite_figure = kwargs.get("overwrite_figure", False)
    overwrite_data = kwargs.get("overwrite_data", False)

    x1v = retrieve_coords(setup, times[0], overwrite_data=overwrite_data)["x1v"]
    time_dict = retrieve_time_dict(setup, overwrite_data=overwrite_data)

    for time in times:
        figure_name += "-{:05d}".format(time)
        # ----- First have to get data ------
        mass_flux_over_r = retrieve_flux_values_over_r("mass_flux", setup, time, overwrite_data=overwrite_data)
        sim_time = time_dict[time]
        label = "$t={:.2f}~GM/c^3$".format(sim_time)

        # ----- Now the actual plotting ------
        # Remember mdot = - mass flux
        plt.plot(x1v, -1.0*mass_flux_over_r, marker="o", markersize=3, label=label)

    plt.xlabel(r"$r/r_g$")
    plt.ylabel(r"$\dot M$")
    plt.gca().axhline([0], color='black', linestyle='--')

    titstr = setup.sim_name
    titstr += "\n Accretion rate through each radial shell"
    plt.legend()
    plt.title(titstr)
    plt.tight_layout()

    figure_name = kwargs.get("figure_name", figure_name)
    if figure_name == "show":
        plt.show()
    else:
        if not os.path.isdir(figure_dir):
            os.makedirs(figure_dir)
        plt.savefig(figure_name + ".png", bbox_inches='tight')
        print("Saving figure " + figure_name + ".png")

    plt.close()


if __name__ == '__main__':
    configuration = "lia-hp"
    project = "ellie"
    config = "1.1.1-torus2_b-gz2"
    specs = "a0beta500torBeta_br32x32x64rl2x2"
    sim_name = "1.1.1-torus2_b-gz2_a0beta500torBeta_br32x32x64rl2x2"
    overwrite_data = False

    setup = setup_manager(configuration, project, sim_name)
    times = get_list_of_time_steps(setup)[::-10]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data

    plot_mdot_over_r(setup, times, **kwargs)

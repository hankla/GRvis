import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from raw_data_utils import *
from reduce_data_utils import *
from metric_utils import *
from calculate_data_utils import *
from plotting_utils import *
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle

def plot_quality_factor_phi_slice(setup, time, **kwargs):
    """
    A script to calculate and plot a slice of the
    toroidal quality factor at a given time.
    Plotting capabilities are taken from Athena++'s plot_spherical.py.
    Equation for Qphi taken from White, Quataert, and Blaes 2019 Eq. 1.

    INPUTS:
    - setup: a setup_manager that has all the path information.
    - time: the timestep to be loaded, i.e. an integer.

    TO DO:
    - add option to time average
    - build save figure path (lots of options...)

    """

    quality_factor_phi_slice = retrieve_quality_factor_phi_slice(setup, time, **kwargs)
    coords = retrieve_coords(setup, time, **kwargs)
    time_dict = retrieve_time_dict(setup)
    sim_time = time_dict[time]

    data = coords
    data["quantity"] = quality_factor_phi_slice

    kwargs['title_str'] = setup.sim_name + "\n Toroidal quality factor slice\n $t=${:.2f}$~GM/c^3$".format(sim_time)
    kwargs['cbar_label'] = r"$Q_\phi$"

    # plt.figure()
    # x1v = coords["x1v"]
    # nx2 = coords["x2v"].size
    # plt.plot(x1v, quality_factor_phi[0, int(nx2/2), :])
    # plt.title("Toroidal quality factor radial profile at midplane\n $t=${:.2f}$~GM/c^3$".format(sim_time))
    # plt.xlabel("$r$")
    # plt.ylabel(r"$Q_\phi$")
#
    # plt.show()
    kwargs = construct_slices_figure_name_from_kwargs(**kwargs)
    plot_slice(data, **kwargs)


if __name__ == '__main__':
    # XX do the output file/show thing, etc.
    # add to plot_multiple_simulation

    # ----- inputs ----------
    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torB_br32x32x64rl2x2"
    # sim_name = config + "_" + specs
    overwrite_data = False

    configuration = "stampede2"
    project = "torus_toroidal-flux"
    sim_name = "19.0-gr-torus2_gz4_a85beta500torBeta_br32x32x64rl2x3"

    # configuration = "stampede2"
    # project = "torus_vert-flux"
    # config = "vert-flux"
    # specs = "initial"

    setup = setup_manager(configuration, project, sim_name)
    times = get_list_of_time_steps(setup)
    times = times[-2:]

    # ------ kwargs --------
    kwargs = {}
    kwargs["overwrite_data"] = overwrite_data
    kwargs['output_file'] = 'show'
    kwargs["midplane"] = False
    kwargs['r_max'] = None
    kwargs['logr'] = False
    kwargs['average'] = False
    kwargs['colormap'] = 'viridis'
    kwargs['vmin'] = None
    kwargs['vmax'] = None
    kwargs['logc'] = False
    kwargs['stream_average'] = False
    kwargs["stream"] = None
    kwargs['setup_val'] = setup
    kwargs['quantity_val'] = "quality-factor-phi"

    # raw_data_path = "/scratch/06165/ahankla/torus_vert-flux/data_raw/vert-flux_initial/"
    # reduced_data_path = "/work/06165/ahankla/stampede2/torus_vert-flux/data_reduced/vert-flux_initial/"

    for time in times:
        kwargs['time_val'] = time
        plot_quality_factor_phi_slice(setup, time, **kwargs)

"""
For every (given) simulation, this script will plot:
    - radial profiles of shell-integrated fluxes (mass flux, angular momentum flux, etc.)
    - 2d slices of relevant output quantities (rho, press)
    - 2d slices of phi quality factor
for every (given) simulation.
"""

import sys
sys.path.append("../scripts/")
sys.path.append("../scripts/modules/")
import os
import matplotlib
matplotlib.use('Agg')
import numpy as np
import raw_data_utils as rdu
import reduce_data_utils as red
from setup_manager import *
from plotting_utils import *
from plot_mag_flux_over_r import plot_mag_flux_over_r
from plot_mdot_over_r import plot_mdot_over_r
from plot_norm_ang_mom_flux_over_r import plot_norm_ang_mom_flux_over_r
from plot_norm_energy_flux_over_r import plot_norm_energy_flux_over_r
from plot_quality_factor_phi_slice import plot_quality_factor_phi_slice


# ----------------------------------------------------
# User configuration
# -----------------------------------------------------
configuration = "lia-d"
project = "torus_toroidal-flux"
config = "19.0-gr-torus2_gz4"
specs = "a85beta500torBeta_br32x32x64rl2x3"
list_of_sims = [config + "_" + specs]

# configuration = "lia-hp" #stampede2 # ellie-hp
# list_of_sims = rdu.get_list_of_sims(setup.path_to_raw_data)
# list_of_sims = ["1.1.1-torus2_b-gz2_a0beta500torBeta_br32x32x64rl2x2"]

# configuration = "stampede2"
# project = "torus_toroidal-flux"
# list_of_sims = ["19.0-gr-torus2_gz4_a85beta500torBeta_br32x32x64rl2x3"]

# configuration = "stampede2"
# project = "torus_vert-flux"
# list_of_sims = ["vert-flux_initial"]

# ----------------------------------------------------
# Inputs on what to plot
# -----------------------------------------------------
# fluxes_to_plot = []
fluxes_to_plot = ["mass_flux", "mag_flux", "norm_ang_mom_flux", "norm_energy_flux"]
flux_times = None
# --------------------------------------------------
plot_slices = True
# the other output quantities are just going to be transformed...
# quantities_to_plot =  ["rho", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3", "press"]
quantities_to_plot =  ["rho", "quality_factor_phi"]
# if slice_times is None, will automatically plot
# every 10th slice, starting from the last.
# slice_times is an array of integers (time steps)
slice_times = np.arange(0, 800, 100)
slice_times = None
# --------------------------------------------------

# ----------------------------------------------------
# Plot for every simulation
# -----------------------------------------------------
for sim in list_of_sims:
    setup = setup_manager(configuration, project, sim)
    print("plotting simulation " + sim)
    sim_times = red.get_list_of_time_steps(setup)
    if flux_times is None:
        flux_times = sim_times
    # Don't plot every single time step--that would be overwhelming!
    if len(flux_times) > 10:
        flux_times = flux_times[::int(len(flux_times)/10)]
        print('Warning: cut down flux times to only plot 10 times.')
    else:
        flux_times = flux_times

    # plot fluxes as a function of radius
    flux_path = setup.path_to_figures + "fluxes_over_r/"
    if not os.path.isdir(flux_path):
        os.makedirs(flux_path)
    if "mass_flux" in fluxes_to_plot:
        print("Plotting mass flux")
        plot_mdot_over_r(setup, flux_times)
    if "mag_flux" in fluxes_to_plot:
        print("Plotting magnetic field flux")
        plot_mag_flux_over_r(setup, flux_times)
    if "norm_ang_mom_flux" in fluxes_to_plot:
        print("Plotting normalized angular momentum flux")
        plot_norm_ang_mom_flux_over_r(setup, flux_times)
    if "norm_energy_flux" in fluxes_to_plot:
        print("Plotting normalized energy flux")
        plot_norm_energy_flux_over_r(setup, flux_times)

    # Plot output quantity slices
    if plot_slices:
        # ------ kwargs --------
        kwargs = {}
        kwargs['setup_val'] = setup
        kwargs["midplane"] = False
        kwargs['r_max'] = 25
        kwargs['logr'] = False
        kwargs['average'] = False
        kwargs['colormap'] = 'viridis'
        kwargs['vmin'] = None
        kwargs['vmax'] = None
        kwargs['logc'] = True
        kwargs['stream_average'] = False
        kwargs["stream"] = None
        kwargs["quantities"] = quantities_to_plot
        if slice_times is None:
            slice_times = sim_times[::-10]
        for time in slice_times:
            print("plotting time {}".format(time))
            kwargs['time_val'] = time
            for quantity in quantities_to_plot:
                kwargs['quantity_val'] = quantity
                kwargs = construct_slices_figure_name_from_kwargs(**kwargs)
                if quantity in Constants.output_quantities:
                    plot_output_quantity_slice(setup, time, quantity, **kwargs)
                elif quantity == "quality_factor_phi":
                    plot_quality_factor_phi_slice(setup, time, **kwargs)

print("Plotting fluxes, output slices, quality factor slices for simulations: ")
print(", ".join(list_of_sims))


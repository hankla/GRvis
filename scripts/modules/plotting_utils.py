import numpy as np
import os
import sys
sys.path.append('../modules/')
from raw_data_utils import *
from metric_utils import *
from calculate_data_utils import *
from plotting_utils import *
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

class Constants:
    output_quantities = ["rho", "press", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]

def plot_slice(data, **kwargs):
    """
    A function for plotting Athena++ data.
    Adapted from Athena++ 19.0 vis/python/plot_spherical.py 2020-05-01.

    INPUTS:
    - data: a dictionary that must contain all the xiv, xif, and also the
            quantity to be plotted, put under data['quantity'] = 3D array.
    - kwargs: mostly the same as plot_spherical.
        - output_file: if "show", just plt.show(). Otherwise, save to the
            given file name.
        - midplane: if True, will take the two cells neighboring the midplane
            and average them. If false, will take a vertical slice.
        - r_max: the coordinate limits, i.e. where to cut off the plot.
        - logr: whether or not to plot r on a log scale. Useful for large
            simulation domains.
        - average: if True, azimuthally average the data before plotting.
        - colormap: which colormap to use
        - vmin/vmax: limits of the colorbar
        - logc: if True, put the colorbar on a log scale
        - stream: if True, plot streamlines
        - stream_average: I haven't bothered to figure this out yet.

    """
    # Load function for transforming coordinates
    if kwargs['stream'] is not None:
        from scipy.ndimage import map_coordinates

    # Load Python plotting modules
    if kwargs['output_file'] != 'show':
        matplotlib.use('agg')
    else:
        matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors

    # Determine if vector quantities should be read
    quantities = ['quantity']
    if kwargs['stream'] is not None:
        quantities.append(kwargs['stream'] + '1')
        if kwargs['midplane']:
            quantities.append(kwargs['stream'] + '3')
        else:
            quantities.append(kwargs['stream'] + '2')

    # Define grid compression in theta-direction
    h = 1.0

    def theta_func(xmin, xmax, _, nf):
        x2_vals = np.linspace(xmin, xmax, nf)
        theta_vals = x2_vals + (1.0 - h) / 2.0 * np.sin(2.0 * x2_vals)
        return theta_vals

    # Extract basic coordinate information
    r = data['x1v']
    theta = data['x2v']
    phi = data['x3v']
    r_face = data['x1f']
    theta_face = data['x2f']
    phi_face = data['x3f']
    nx1 = len(r)
    nx2 = len(theta)
    nx3 = len(phi)

    # Set radial extent
    if kwargs['r_max'] is not None:
        r_max = kwargs['r_max']
    else:
        r_max = r_face[-1]

    # Account for logarithmic radial coordinate
    if kwargs['logr']:
        r = np.log10(r)
        r_face = np.log10(r_face)
        r_max = np.log10(r_max)

    # Create scalar grid
    if kwargs['midplane']:
        r_grid, phi_grid = np.meshgrid(r_face, phi_face)
        x_grid = r_grid * np.cos(phi_grid)
        y_grid = r_grid * np.sin(phi_grid)
    else:
        theta_face_extended = np.concatenate((theta_face, 2.0*np.pi - theta_face[-2::-1]))
        r_grid, theta_grid = np.meshgrid(r_face, theta_face_extended)
        x_grid = r_grid * np.sin(theta_grid)
        y_grid = r_grid * np.cos(theta_grid)

    # Create streamline grid
    if kwargs['stream'] is not None:
        x_stream = np.linspace(-r_max, r_max, kwargs['stream_samples'])
        if kwargs['midplane']:
            y_stream = np.linspace(-r_max, r_max, kwargs['stream_samples'])
            x_grid_stream, y_grid_stream = np.meshgrid(x_stream, y_stream)
            r_grid_stream_coord = (x_grid_stream.T**2 + y_grid_stream.T**2) ** 0.5
            phi_grid_stream_coord = np.pi + np.arctan2(-y_grid_stream.T, -x_grid_stream.T)
            phi_grid_stream_pix = ((phi_grid_stream_coord + phi[0])
                                   / (2.0*np.pi + 2.0 * phi[0])) * (nx3 + 1)
        else:
            z_stream = np.linspace(-r_max, r_max, kwargs['stream_samples'])
            x_grid_stream, z_grid_stream = np.meshgrid(x_stream, z_stream)
            r_grid_stream_coord = (x_grid_stream.T**2 + z_grid_stream.T**2) ** 0.5
            theta_grid_stream_coord = np.pi - \
                np.arctan2(x_grid_stream.T, -z_grid_stream.T)
            theta_grid_stream_pix = np.empty_like(theta_grid_stream_coord)
            theta_extended = np.concatenate((-theta[0:1], theta,
                                             2.0*np.pi - theta[::-1],
                                             2.0*np.pi + theta[0:1]))
            for (i, j), theta_val in np.ndenumerate(theta_grid_stream_coord):
                index = sum(theta_extended[1:-1] < theta_val) - 1
                if index < 0:
                    theta_grid_stream_pix[i, j] = -1
                elif index < 2 * nx2 - 1:
                    theta_grid_stream_pix[i, j] = (
                        index + ((theta_val - theta_extended[index])
                                 / (theta_extended[index+1] - theta_extended[index])))
                else:
                    theta_grid_stream_pix[i, j] = 2 * nx2 + 2
        r_grid_stream_pix = np.empty_like(r_grid_stream_coord)
        for (i, j), r_val in np.ndenumerate(r_grid_stream_coord):
            index = sum(r < r_val) - 1
            if index < 0:
                r_grid_stream_pix[i, j] = -1
            elif index < nx1 - 1:
                r_grid_stream_pix[i, j] = index + \
                    (r_val - r[index]) / (r[index + 1] - r[index])
            else:
                r_grid_stream_pix[i, j] = nx1

    # Perform slicing/averaging of scalar data
    if kwargs['midplane']:
        if nx2 % 2 == 0:
            vals = np.mean(data['quantity'][:, int(nx2/2)-1:int(nx2/2)+1, :], axis=1)
        else:
            vals = data['quantity'][:, int(nx2/2), :]
        if kwargs['average']:
            vals = np.repeat(np.mean(vals, axis=0, keepdims=True), nx3, axis=0)
    else:
        if kwargs['average']:
            vals_right = np.mean(data['quantity'], axis=0)
            vals_left = vals_right
        else:
            vals_right = 0.5 * (data['quantity']
                                [-1, :, :] + data['quantity'][0, :, :])
            vals_left = 0.5 * (data['quantity'][int(nx3/2)-1, :, :]
                               + data['quantity'][int(nx3 / 2), :, :])

    # Join scalar data through boundaries
    if not kwargs['midplane']:
        vals = np.vstack((vals_right, vals_left[::-1, :]))

    # Perform slicing/averaging of vector data
    if kwargs['stream'] is not None:
        if kwargs['midplane']:
            if nx2 % 2 == 0:
                vals_r = np.mean(data[kwargs['stream'] + '1']
                                 [:, nx2/2-1:nx2/2+1, :], axis=1).T
                vals_phi = np.mean(data[kwargs['stream'] + '3']
                                   [:, nx2/2-1:nx2/2+1, :], axis=1).T
            else:
                vals_r = data[kwargs['stream'] + '1'][:, nx2/2, :].T
                vals_phi = data[kwargs['stream'] + '3'][:, nx2/2, :].T
            if kwargs['stream_average']:
                vals_r = np.tile(np.reshape(np.mean(vals_r, axis=1), (nx1, 1)), nx3)
                vals_phi = np.tile(np.reshape(np.mean(vals_phi, axis=1), (nx1, 1)), nx3)
        else:
            if kwargs['stream_average']:
                vals_r_right = np.mean(data[kwargs['stream'] + '1'], axis=0).T
                vals_r_left = vals_r_right
                vals_theta_right = np.mean(data[kwargs['stream'] + '2'], axis=0).T
                vals_theta_left = -vals_theta_right
            else:
                vals_r_right = data[kwargs['stream'] + '1'][0, :, :].T
                vals_r_left = data[kwargs['stream'] + '1'][nx3/2, :, :].T
                vals_theta_right = data[kwargs['stream'] + '2'][0, :, :].T
                vals_theta_left = -data[kwargs['stream'] + '2'][nx3/2, :, :].T

    # Join vector data through boundaries
    if kwargs['stream'] is not None:
        if kwargs['midplane']:
            vals_r = np.hstack((vals_r[:, -1:], vals_r, vals_r[:, :1]))
            vals_r = map_coordinates(vals_r, (r_grid_stream_pix, phi_grid_stream_pix),
                                     order=1, cval=np.nan)
            vals_phi = np.hstack((vals_phi[:, -1:], vals_phi, vals_phi[:, :1]))
            vals_phi = map_coordinates(vals_phi, (r_grid_stream_pix, phi_grid_stream_pix),
                                       order=1, cval=np.nan)
        else:
            vals_r = np.hstack((vals_r_left[:, :1], vals_r_right, vals_r_left[:, ::-1],
                                vals_r_right[:, :1]))
            vals_r = map_coordinates(vals_r, (r_grid_stream_pix, theta_grid_stream_pix),
                                     order=1, cval=np.nan)
            vals_theta = np.hstack((vals_theta_left[:, :1], vals_theta_right,
                                    vals_theta_left[:, ::-1], vals_theta_right[:, :1]))
            vals_theta = map_coordinates(vals_theta,
                                         (r_grid_stream_pix, theta_grid_stream_pix),
                                         order=1, cval=np.nan)

    # Transform vector data to Cartesian components
    if kwargs['stream'] is not None:
        if kwargs['logr']:
            r_vals = 10.0**r_grid_stream_coord
            logr_vals = r_grid_stream_coord
        else:
            r_vals = r_grid_stream_coord
        if kwargs['midplane']:
            sin_phi = np.sin(phi_grid_stream_coord)
            cos_phi = np.cos(phi_grid_stream_coord)
            if kwargs['logr']:
                dx_dr = 1.0 / (np.log(10.0) * r_vals) * cos_phi
                dy_dr = 1.0 / (np.log(10.0) * r_vals) * sin_phi
                dx_dphi = -logr_vals * sin_phi
                dy_dphi = logr_vals * cos_phi
            else:
                dx_dr = cos_phi
                dy_dr = sin_phi
                dx_dphi = -r_vals * sin_phi
                dy_dphi = r_vals * cos_phi
            vals_x = dx_dr * vals_r + dx_dphi * vals_phi
            vals_y = dy_dr * vals_r + dy_dphi * vals_phi
        else:
            sin_theta = np.sin(theta_grid_stream_coord)
            cos_theta = np.cos(theta_grid_stream_coord)
            if kwargs['logr']:
                dx_dr = 1.0 / (np.log(10.0) * r_vals) * sin_theta
                dz_dr = 1.0 / (np.log(10.0) * r_vals) * cos_theta
                dx_dtheta = logr_vals * cos_theta
                dz_dtheta = -logr_vals * sin_theta
            else:
                dx_dr = sin_theta
                dz_dr = cos_theta
                dx_dtheta = r_vals * cos_theta
                dz_dtheta = -r_vals * sin_theta
            vals_x = dx_dr * vals_r + dx_dtheta * vals_theta
            vals_z = dz_dr * vals_r + dz_dtheta * vals_theta

    # Determine colormapping properties
    kw_cmap = kwargs.get("colormap", "viridis")
    cmap = plt.get_cmap(kw_cmap)
    vmin = kwargs['vmin']
    vmax = kwargs['vmax']
    if kwargs['logc']:
        norm = colors.LogNorm()
    else:
        norm = colors.Normalize()

    # Make plot
    plt.figure()
    im = plt.pcolormesh(x_grid, y_grid, vals, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)
    if kwargs['stream'] is not None:
        with warnings.catch_warnings():
            warnings.filterwarnings(
                'ignore',
                'invalid value encountered in greater_equal',
                RuntimeWarning,
                'numpy')
            if kwargs['midplane']:
                plt.streamplot(x_stream, y_stream, vals_x.T, vals_y.T,
                               density=kwargs['stream_density'], color='k')
            else:
                plt.streamplot(x_stream, z_stream, vals_x.T, vals_z.T,
                               density=kwargs['stream_density'], color='k')
    plt.gca().set_aspect('equal')
    plt.xlim((-r_max, r_max))
    plt.ylim((-r_max, r_max))
    if kwargs['logr']:
        if kwargs['midplane']:
            plt.xlabel(r'$\log_{10}(r)\ x / r$')
            plt.ylabel(r'$\log_{10}(r)\ y / r$')
        else:
            plt.xlabel(r'$\log_{10}(r)\ x / r$')
            plt.ylabel(r'$\log_{10}(r)\ z / r$')
    else:
        if kwargs['midplane']:
            plt.xlabel(r'$x$')
            plt.ylabel(r'$y$')
        else:
            plt.xlabel(r'$x$')
            plt.ylabel(r'$z$')
    cbar = plt.colorbar(im)
    cbar.set_label(kwargs['cbar_label'])
    plt.title(kwargs['title_str'])
    plt.tight_layout()

    if kwargs['output_file'] == 'show':
        plt.show()
    else:
        plt.savefig(kwargs['output_file'], bbox_inches='tight')
    plt.close()

def plot_output_quantity_slice(setup_manager, time, quantity, **kwargs):
    """
    A script to calculate and plot a slice of the
    given output quantity at a given time.

    INPUTS:
    - setup_manager: contains all the relevant paths. See modules/setup_manager
    - time: the timestep to be loaded, i.e. an integer.
    - quantity: must be directly output by Athena++.

    TO DO:
    - add option to load from file instead of calculating?
    """
    # load_from_file = kwargs.get("load_from_file", True)
    # write_to_file = kwargs.get("write_to_file", True)

    output_quantities = ["rho", "press", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]
    quantity_names = {"rho": r"$\rho$", "vel1": r"$v_r$", "vel2":r"$v_\theta$", "vel3":r"$v_\phi$", "press":"$P$", "Bcc1":"$B_r$", "Bcc2":r"$B_\theta$", "Bcc3":r"$B_\phi$"}

    raw_data_path = setup_manager.path_to_raw_data
    reduced_data_path = setup_manager.path_to_reduced_data
    figure_path = setup_manager.path_to_figures
    if not os.path.exists(reduced_data_path):
        os.makedirs(reduced_data_path)
    if not os.path.exists(figure_path):
        os.makedirs(figure_path)

    # ----- First have to get data ------
    filename = raw_data_path + setup_manager.sim_name + ".prim.{:05}.athdf".format(time)

    if quantity not in output_quantities:
        print("ERROR: " + quantity + " is not an output quantity.")
        print("Output quantities: " + ', '.join(quantities))
    raw_data = read_athdf(filename, quantities=[quantity])
    sim_time = raw_data["Time"]
    x1v = raw_data["x1v"]
    x2v = raw_data["x2v"]; nx2 = x2v.size
    x3v = raw_data["x3v"]; nx3 = x3v.size

    data = {"quantity":raw_data[quantity], "x1v":x1v, "x2v":x2v, "x3v":x3v, "x1f":raw_data["x1f"], "x2f":raw_data["x2f"], "x3f":raw_data["x3f"]}

    kwargs['title_str'] = setup_manager.sim_name + "\n" + quantity_names[quantity] + "\n $t=${:.2f}$GM/c^3$".format(sim_time)
    kwargs['cbar_label'] = quantity_names[quantity]

    plot_slice(data, **kwargs)


def construct_slices_figure_name_from_kwargs(**kwargs):
    setup = kwargs['setup_val']
    quantity = kwargs['quantity_val']
    time = kwargs['time_val']

    figure_dir = setup.path_to_figures + "slices/"
    if kwargs["midplane"]:
        figure_dir += "midplane/"
    else:
        figure_dir += "vertical/"
    figure_dir += quantity + "/"
    if kwargs['logc']:
        figure_dir += "logc/"
    if kwargs['vmin'] is not None or kwargs['vmax'] is not None:
        figure_dir += "cfixed{:.2f}-{:.2f}/".format(kwargs['vmin'], kwargs['vmax'])
    if kwargs["average"]:
        figure_dir += "average/"

    if not os.path.isdir(figure_dir):
        os.makedirs(figure_dir)

    figure_name = figure_dir + quantity + "_t{:05d}".format(int(time))
    if kwargs["midplane"]:
        figure_name += "_midplane"
    else:
        figure_name += "_vertical"
    if kwargs['vmin'] is not None or kwargs['vmax'] is not None:
        figure_name += "_cfixed{:.2f}-{:.2f}".format(kwargs['vmin'], kwargs['vmax'])
    figure_name += ".png"
    kwargs['output_file'] = figure_name
    return kwargs

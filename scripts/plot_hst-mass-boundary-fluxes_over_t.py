import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from reduce_data_utils import *
from raw_data_utils import hst
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_hst_mass_boundary_fluxes_over_t(setup, **kwargs):
    """
    This script will calculate the amount of mass accreted
    through the inner and outer boundary over all time
    and compare it to the .hst file.

    INPUTS:
    - setup: a setup_manager that has all the path information.
    KWARGS:
    - times: time steps to plot.
    - overwrite_figure: if True, then overwrite any figure with the same name.
        Otherwise, check if figure exists and don't overwrite.
        XX NOT IMPLEMENTED YET XX
    - figure_name: if specified, save figure with this name. If not specified,
        save figure will pre-built name. If "show", then plt.show().

    TO DO:
    - overwrite_figure
    """

    overwrite_figure = kwargs.get("overwrite_figure", False)
    overwrite_data = kwargs.get("overwrite_data", False)

    time_dict = retrieve_time_dict(setup, overwrite_data=overwrite_data)
    given_times = kwargs.get("times", None)
    times_to_plot = {}
    radius_values = retrieve_coords(setup)["x1v"]
    flux_type = "hst-mass-boundary-fluxes"

    figure_dir = setup.path_to_figures + "fluxes_over_t/"
    figure_name = figure_dir + flux_type + "_over_t"

    # Load from .hst file
    hst_data = hst(setup.hst_file)
    hst_mass_over_time = hst_data["mass"]
    hst_times = hst_data["time"]

    if given_times is not None:
        for time in given_times:
            times_to_plot[time] = time_dict[time]
    else:
        times_to_plot = time_dict

    time_values = []
    mass_flux_through_inner_r = []
    mass_flux_through_outer_r = []

    for time_step in times_to_plot.keys():
        mass_flux_over_r = retrieve_flux_values_over_r("mass_flux", setup, time_step, **kwargs)
        mass_flux_through_inner_r.append(mass_flux_over_r[0])
        mass_flux_through_outer_r.append(mass_flux_over_r[-1])
        time_values.append(time_dict[time_step])

    mass_flux_through_inner_r = np.array(mass_flux_through_inner_r)
    mass_flux_through_outer_r = np.array(mass_flux_through_outer_r)
    total_mass_lost_through_boundaries = mass_flux_through_inner_r - mass_flux_through_outer_r
    total_mass_lost_hst = 1.0*np.diff(hst_mass_over_time, prepend=hst_mass_over_time[0])

    figure_name = kwargs.get("figure_name", figure_name)
    if figure_name == "show":
        matplotlib.use('TkAgg')

    plt.figure()
    plt.plot(time_values, mass_flux_through_inner_r, marker="o", markersize=3, label="Inner")
    plt.plot(time_values, mass_flux_through_outer_r, marker="o", markersize=3, label="Outer")
    plt.plot(time_values, total_mass_lost_through_boundaries, marker="o", markersize=3, label="Inner - Outer")
    plt.plot(hst_times, total_mass_lost_hst, marker="+", markersize=3, label="hst")
    plt.legend()
    plt.xlabel(r"$t/(r_g/c)$")
    plt.ylabel(r"$\dot M$")

    titstr = setup.sim_name
    plt.title(titstr)
    plt.tight_layout()

    if figure_name == "show":
        print("showing")
        plt.show()
    else:
        if not os.path.isdir(figure_dir):
            os.makedirs(figure_dir)
        print("Saving figure " + figure_name + ".png")
        plt.savefig(figure_name + ".png", bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torBeta_br32x32x64rl2x2"
    # sim_name = "1.1.1-torus2_b-gz2_a0beta500torBeta_br32x32x64rl2x2"

    configuration = "lia-d"
    project = "torus_toroidal-flux"
    config = "19.0-gr-torus2_gz4"
    specs = "a85beta500torBeta_br32x32x64rl2x3"
    sim_name = config + "_" + specs

    overwrite_data = False

    setup = setup_manager(configuration, project, sim_name)
    time_steps = get_list_of_time_steps(setup)
    time_steps = np.arange(0, 1000)

    kwargs = {}
    # kwargs["figure_name"] = "show"

    kwargs["overwrite_data"] = overwrite_data
    kwargs["times"] = time_steps

    plot_hst_mass_boundary_fluxes_over_t(setup, **kwargs)

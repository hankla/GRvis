#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH --partition=development
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=multipleSims
#SBATCH --output=logs/out_plot_analysis.%j
#SBATCH --error=logs/err_plot_analysis.%j
#SBATCH -A TG-AST190020

# --------------------------------------
# USAGE
# 1. Edit ../scripts/plot_multiple-simulations.py to desired specifications.
# 2. Change above SBATCH settings.
# 3. Make sure logs exists in this directory! i.e. run "mkdir -p logs"
# 4. From the command line (in the sbatch_scripts/ directory), run
#         sbatch sbatch_plot_multiple_simulations.sh
# --------------------------------------

# --------------------------------------
# NO USER INPUTS
# NOTE that all parameters must be changed within the
# ../scripts/plot_multiple-simulations.py file!!!
# --------------------------------------

# --------------------------------------
# NO EDITING NEEDED BELOW THIS LINE
# --------------------------------------
module purge
module load intel/18.0.2
module load python3/3.7.0
module list

scriptpath="$HOME/GRvis/scripts/"
logpath=$(pwd)"/logs/log_${script}.txt"
fname="plot_multiple-simulations.py"

cd ${scriptpath}
pwd
date
python3 -u ${fname} > ${logpath}
date

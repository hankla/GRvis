import os
import sys

def make_script_copy(script_name, new_project, new_sim_name):
    """
    This script will make a copy of the script script_name,
    but with the following changes:
    configuration --> "stampede2"
    project --> new_project
    sim_name --> new_sim_name
    """
    print(new_project)
    print(new_sim_name)
    changed_config = False
    changed_project = False
    changed_sim_name = False

    # first, find script.
    # assume was launched from sbatch_scripts/ directory.
    # script_name must include .py extension.
    os.chdir('../scripts/')
    if not os.path.isdir('script_copies/'):
        os.mkdir('script_copies/')
    script_path = os.getcwd() + "/" + script_name
    new_script_path = os.getcwd() + "/script_copies/" + script_name[:-3] + "_copy.py"
    print(new_script_path)

    with open(new_script_path, "w") as new_script_file:
        with open(script_path, "r") as script_file:
            for line in script_file:
                line_copy = line.strip(' ')
                # first, make sure configuration is set to stampede2
                # (since that's the only place we'll do sbatch)
                if line_copy.startswith('configuration ='):
                    old_config = line_copy.split(" ")[-1]
                    line = line.replace(old_config, '"stampede2"\n')
                    changed_config = True
                # Now, change project name
                elif line_copy.startswith('project ='):
                    old_project = line_copy.split(" ")[-1]
                    line = line.replace(old_project, '"' + new_project + '"\n')
                    changed_project = True
                # Now, change simulation name
                elif line_copy.startswith('sim_name ='):
                    old_sim_name = line_copy.split(" ")[-1]
                    line = line.replace(old_sim_name, '"' + new_sim_name + '"\n')
                    changed_sim_name = True

                # Write line to file with all its replacements
                new_script_file.write(line)
    if not (changed_config and changed_project and changed_sim_name):
        print("-------------------------------------------------")
        print("ERROR:")
        print("changed configuration to stampede2: " + str(changed_config))
        print("changed project to " + new_project + ": " + str(changed_project))
        print("changed sim_name to " + new_sim_name + ": " + str(changed_sim_name))
        print("-------------------------------------------------")
        return 0

    return new_script_path

if __name__ == '__main__':
    make_script_copy(*sys.argv[1:])

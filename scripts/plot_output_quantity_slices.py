import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from setup_manager import *
from plotting_utils import *
from reduce_data_utils import get_list_of_time_steps
import numpy as np


if __name__ == '__main__':
    # ----- inputs ----------
    configuration = "lia-d"
    project = "torus_toroidal-flux"
    config = "19.0-gr-torus2_gz4"
    specs = "a85beta500torBeta_br32x32x64rl2x3"
    sim_name = config + "_" + specs

    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torB_br32x32x64rl2x2"
    # sim_name = config + "_" + specs

    # configuration = "stampede2"
    # project = "torus_toroidal-flux"
    # sim_name = "19.0-gr-torus2_gz4_a85beta500torBeta_br32x32x64rl2x3"

    # quantities_to_plot =  ["rho", "press", "vel1", "vel2", "vel3", "Bcc1", "Bcc2", "Bcc3"]
    quantities_to_plot =  ["rho", "press"]

    setup = setup_manager(configuration, project, sim_name)
    times = get_list_of_time_steps(setup)
    times = np.arange(0, 1000, 10)
    times = np.arange(400, 1200, 10)

    # ------ kwargs --------
    kwargs = {}
    kwargs["midplane"] = True
    kwargs['r_max'] = 25
    kwargs['logr'] = False
    kwargs['average'] = False
    kwargs['colormap'] = 'viridis'
    kwargs['vmin'] = 0.02
    kwargs['vmax'] = 0.7
    kwargs['logc'] = True
    kwargs['stream_average'] = False
    kwargs["stream"] = None
    kwargs["quantities"] = quantities_to_plot
    kwargs['setup_val'] = setup

    for time in times:
        print("plotting time {}".format(time))
        kwargs['time_val'] = time
        for quantity in quantities_to_plot:
            kwargs['quantity_val'] = quantity
            kwargs = construct_slices_figure_name_from_kwargs(**kwargs)
            plot_output_quantity_slice(setup, time, quantity, **kwargs)

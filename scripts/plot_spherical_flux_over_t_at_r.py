import numpy as np
import sys
sys.path.append('modules/')
sys.path.append('../modules/')
from reduce_data_utils import *
from setup_manager import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_spherical_flux_over_t_at_r(setup, radius_ind, flux_type, **kwargs):
    """
    This script will calculate (if necessary) and plot the
    specified spherical flux (integrated over radial shells)
    at a given time radius over time.

    INPUTS:
    - setup: a setup_manager that has all the path information.
    - radius_ind: the index (not the value) of the radius at which to plot the mass flux
    - flux_type: options are "mass_flux", mag_flux", "norm_ang_mom_flux", "norm_energy_flux"
    KWARGS:
    - overwrite_data: if True, then re-calculate/overwrite the data.
        Otherwise, try to load data from a file if it exists.
    - overwrite_figure: if True, then overwrite any figure with the same name.
        Otherwise, check if figure exists and don't overwrite.
        XX NOT IMPLEMENTED YET XX
    - figure_name: if specified, save figure with this name. If not specified,
        save figure will pre-built name. If "show", then plt.show().

    TO DO:
    - save data
    - show doesn't work
    """
    flux_labels = {"mass_flux": r"$-\dot M$", "mag_flux":r"$\Phi$", "norm_ang_mom_flux":r"$\dot J/\dot M$", "norm_energy_flux":r"$\dot E/\dot M$"}
    flux_strings = {"mass_flux": "Mass Flux (-1.0 * Accretion Rate)", "mag_flux":"Magnetic Flux", "norm_ang_mom_flux":"Angular momentum flux (normalized to mass flux)", "norm_energy_flux":r"Energy flux (normalized to mass flux)"}

    overwrite_figure = kwargs.get("overwrite_figure", False)
    overwrite_data = kwargs.get("overwrite_data", False)

    time_dict = retrieve_time_dict(setup, overwrite_data=overwrite_data)
    times_to_plot = {}
    radius_values = retrieve_coords(setup)["x1v"]

    figure_dir = setup.path_to_figures + "fluxes-spherical_over_t_at_r/"
    figure_dir += flux_type + "/"
    figure_name = figure_dir + flux_type + "_through_shells_over_t_at_r{:.2f}".format(radius_values[radius_ind])
    figure_name = kwargs.get("figure_name", figure_name)

    if kwargs['times'] is not None:
        for time in kwargs['times']:
            times_to_plot[time] = time_dict[time]
    else:
        times_to_plot = time_dict

    time_values = []
    flux_values_at_r = []

    for time_step in times_to_plot.keys():
        flux_values_at_r.append(retrieve_spherical_flux_values_over_r(flux_type, setup, time_step, **kwargs)[radius_ind])
        time_values.append(time_dict[time_step])

    flux_values_at_r = np.array(flux_values_at_r)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.plot(time_values, flux_values_at_r, marker="o", markersize=3)
    plt.xlabel(r"$t/(r_g/c)$")
    plt.ylabel(flux_labels[flux_type])
    titstr = setup.sim_name
    titstr += "\n" + flux_strings[flux_type]
    titstr += " through $r={:.2f}$ (index: {}) over time".format(radius_values[radius_ind], radius_ind)
    plt.title(titstr)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        if not os.path.isdir(figure_dir):
            os.makedirs(figure_dir)
        print("Saving figure " + figure_name + ".png")
        plt.savefig(figure_name + ".png", bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    # configuration = "lia-hp"
    # project = "ellie"
    # config = "1.1.1-torus2_b-gz2"
    # specs = "a0beta500torBeta_br32x32x64rl2x2"
    # sim_name = "1.1.1-torus2_b-gz2_a0beta500torBeta_br32x32x64rl2x2"

    configuration = "lia-d"
    project = "torus_toroidal-flux"
    config = "19.0-gr-torus2_gz4"
    specs = "a85beta500torBeta_br32x32x64rl2x3"
    sim_name = config + "_" + specs
    # flux_type = "mass_flux"
    flux_type = "norm_energy_flux"

    overwrite_data = False

    setup = setup_manager(configuration, project, sim_name)
    time_steps = get_list_of_time_steps(setup)[:1280]
    r_vals = retrieve_coords(setup) ["x1v"]
    # radius_value = 10.0
    # radius_ind = (np.abs(r_vals - radius_value)).argmin()
    radius_ind = 0

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["times"] = time_steps

    plot_spherical_flux_over_t_at_r(setup, radius_ind, flux_type, **kwargs)
